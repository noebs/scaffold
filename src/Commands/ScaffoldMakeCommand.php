<?php

namespace Noebs\Scaffold\Commands;

use Illuminate\Console\Command;

use Illuminate\Container\Container;

use Illuminate\Filesystem\Filesystem;

use Symfony\Component\Console\Input\InputOption;

use Symfony\Component\Console\Input\InputArgument;

use Illuminate\Support\Facades\DB;

class ScaffoldMakeCommand extends Command
{

    protected $name = 'noebs:scaffold';

    protected $description = 'Create a new scaffolding ';

    protected $files;

    protected $meta;

    protected $type='Scaffolding';

    public function __construct(Filesystem $files)
    {

        parent::__construct();

        $this->files = $files;

    }

    public function fire()
    {

        $this->makeController();

        $this->makeForm();

        $this->makeModel();

        $this->makeRule();

        $this->makeMapper();

        $this->makeEloquent();

        $this->makeRepository();

        $this->makeProvider();

    }

    protected function getAppNamespace()
    {

        return Container::getInstance()->getNamespace();

    }

    protected function makeDirectory($path)
    {

        if (!$this->files->isDirectory(dirname($path)))
        {

            $this->files->makeDirectory(dirname($path), 0777, true, true);

        }

    }

    protected function makeController()
    {

        $name = $this->argument('name');

        if ($this->files->exists($path = $this->getControllerPath($name)))
        {

            return $this->error('Controller already exists!');

        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->compileControllerStub());

        $this->info('Controller '.$name.' created successfully.');

    }

    protected function getControllerPath($name)
    {

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches)
        {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true")
        {

            return base_path() . '/app/Http/Controllers/'.ucwords(str_plural($name)).'Controller.php';

        }

        return base_path() . '/app/Http/Controllers/'.ucwords(str_singular($name)).'Controller.php';

    }

    protected function compileControllerStub()
    {

        $stub = $this->files->get(__DIR__ . '/../stubs/controller.stub');

        $this->replaceController($stub);

        return $stub;

    }

    protected function replaceController(&$stub)
    {

        if($this->option('plural') == "true")
        {

            $class = ucwords(str_plural(camel_case($this->argument('name'))));

            $obj = snake_case(str_plural(camel_case($this->argument('name'))));

        }else{

            $class = ucwords(str_singular(camel_case($this->argument('name'))));

            $obj = snake_case(str_singular(camel_case($this->argument('name'))));

        }

        $class_alias = ucwords(str_singular(camel_case($this->argument('name'))));

        $stub = str_replace('{{class}}', $class, $stub);

        $stub = str_replace('{{class_alias}}', $class_alias, $stub);

        $stub = str_replace('{{obj}}', $obj, $stub);

        return $this;

    }

    protected function makeForm()
    {

        $name = $this->argument('name');

        if ($this->files->exists($path = $this->getFormPath($name)))
        {

            return $this->error('Form already exists!');

        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->compileFormStub());

        $this->info('Form '.$name.' created successfully.');

    }

    protected function getFormPath($name)
    {

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches)
        {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true")
        {

            return base_path() . '/app/Codes/Forms/' . ucwords(str_plural($name)) . '/Form.php';

        }else{

            return base_path() . '/app/Codes/Forms/' . ucwords(str_singular($name)) . '/Form.php';

        }

    }

    protected function compileFormStub()
    {

        $stub = $this->files->get(__DIR__ . '/../stubs/form.stub');

        $this->replaceForm($stub);

        return $stub;

    }

    protected function replaceForm(&$stub)
    {

        $name = $this->argument('name');

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true") {

            $namespace = 'namespace App\Codes\Forms\\' . ucwords(str_plural($name));

            $import_class_folder = ucwords(str_plural($name));

            $obj = str_plural(strtolower($this->argument('name')));

        }else{

            $namespace = 'namespace App\Codes\Forms\\' . ucwords(str_singular($name));

            $import_class_folder = ucwords(str_singular($name));

            $obj = str_singular(strtolower($this->argument('name')));

        }

        $repository_name = ucwords(str_singular($name)).'Repository';

        $stub = str_replace('{{form_namespace}}', $namespace, $stub);

        $stub = str_replace('{{import_class_folder}}', $import_class_folder, $stub);

        $stub = str_replace('{{repository_name}}', $repository_name, $stub);

        $stub = str_replace('{{obj}}', $obj, $stub);

        return $this;
    }

    protected function makeRule()
    {

        $name = $this->argument('name');

        if ($this->files->exists($path = $this->getRulePath($name))) {

            return $this->error('Rules already exists!');

        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->compileRuleStub());

        $this->info('Rule '.$name.' created successfully.');

    }

    protected function getRulePath($name)
    {

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true") {

            return base_path() . '/app/Codes/Forms/' . ucwords(str_plural($name)) . '/Rules.php';

        }

        return base_path() . '/app/Codes/Forms/' . ucwords(str_singular($name)) . '/Rules.php';
    }

    protected function compileRuleStub()
    {
        $stub = $this->files->get(__DIR__ . '/../stubs/rules.stub');

        $this->replaceRule($stub);

        return $stub;
    }

    protected function replaceRule(&$stub)
    {

        $name = $this->argument('name');

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true") {

            $namespace = 'namespace App\Codes\Forms\\' . ucwords(str_plural($name));

            $table = str_plural(strtolower($this->argument('name')));

        }else{

            $namespace = 'namespace App\Codes\Forms\\' . ucwords(str_singular($name));

            $table = str_singular(strtolower($this->argument('name')));

        }

        $columns = DB::getSchemaBuilder()->getColumnListing($table);

        $insert_rules = '';

        $update_rules = '';

        if(count($columns)>0){

            foreach ($columns as $column){

                if($column !== 'id'){

                    $insert_rules = $insert_rules."'".$column."'=>'',\n";

                }

                $update_rules = $update_rules."'".$column."'=>'',\n";

            }

        }

        $stub = str_replace('{{rules_namespace}}', $namespace, $stub);

        $stub = str_replace('{{update_rules}}', $update_rules, $stub);

        $stub = str_replace('{{insert_rules}}', $insert_rules, $stub);

        return $this;
    }

    protected function makeMapper()
    {
        $name = $this->argument('name');

        if ($this->files->exists($path = $this->getMapperPath($name))) {

            return $this->error('Mapper already exists!');

        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->compileMapperStub());

        $this->info('Mapper '.$name.' created successfully.');

    }

    protected function getMapperPath($name)
    {

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true") {

            return base_path() . '/app/Codes/Mappers/' . ucwords(str_plural($name)) . '/Mapper.php';

        }

        return base_path() . '/app/Codes/Mappers/' . ucwords(str_singular($name)) . '/Mapper.php';


    }

    protected function compileMapperStub()
    {
        $stub = $this->files->get(__DIR__ . '/../stubs/mapper.stub');

        $this->replaceMapper($stub);

        return $stub;
    }

    protected function replaceMapper(&$stub)
    {
        $name = $this->argument('name');

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true") {

            $namespace = 'namespace App\Codes\Mappers\\' . ucwords(str_plural($name));

            $table = str_plural(strtolower($this->argument('name')));

        }else{

            $namespace = 'namespace App\Codes\Mappers\\' . ucwords(str_singular($name));

            $table = str_singular(strtolower($this->argument('name')));

        }

        $columns = DB::getSchemaBuilder()->getColumnListing($table);

        $insert_mapper = '';

        $update_mapper = '';
        
		$search_mapper = '';

        if(count($columns)>0){

            foreach ($columns as $column){

                if($column !== 'id' && $column !== 'created_at' && $column !== 'updated_at' && $column !== 'deleted_at'){

                    $insert_mapper = $insert_mapper."'".$column."',\n";

                }

                if($column !== 'created_at' && $column !== 'updated_at' && $column !== 'deleted_at') {

                    $update_mapper = $update_mapper . "'" . $column . "',\n";

                }
				
				$search_mapper = $search_mapper."'".$column."',\n";

            }

        }

        $stub = str_replace('{{mapper_namespace}}', $namespace, $stub);

        $stub = str_replace('{{update_fields}}', $update_mapper, $stub);

        $stub = str_replace('{{insert_fields}}', $insert_mapper, $stub);
        
		$stub = str_replace('{{search_fields}}', $search_mapper, $stub);

        return $this;
    }

    protected function makeModel()
    {
        $name = $this->argument('name');

        if ($this->files->exists($path = $this->getModelPath($name))) {

            return $this->error('Model already exists!');

        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->compileModelStub());

        $this->info('Model '.$name.' created successfully.');

    }

    protected function getModelPath($name)
    {

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        return base_path() . '/app/Codes/Models/'.ucwords(str_singular($name)).'.php';

    }

    protected function compileModelStub()
    {
        $stub = $this->files->get(__DIR__ . '/../stubs/models.stub');

        $this->replaceModel($stub);

        return $stub;
    }

    protected function replaceModel(&$stub)
    {

        $name = $this->argument('name');

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true") {

            $table = str_plural(strtolower($this->argument('name')));

        }else{

            $table = str_singular(strtolower($this->argument('name')));

        }

        $class_name = str_singular($name);

        $columns = DB::getSchemaBuilder()->getColumnListing($table);

        $fields = '';

        $hidden_fields = '';

        if(count($columns)>0){

            foreach ($columns as $column){

                if($column !== 'created_at' && $column !== 'updated_at' && $column !== 'deleted_at')
                {

                    $fields = $fields . "'" . $column . "',\n";

                }else{
                    $hidden_fields = $hidden_fields . "'" . $column . "',\n";
                }

            }

        }

        $stub = str_replace('{{class_name}}', $class_name, $stub);

        $stub = str_replace('{{fields}}', $fields, $stub);

        $stub = str_replace('{{hidden_fields}}', $hidden_fields, $stub);

        $stub = str_replace('{{table}}', $table, $stub);

        return $this;
    }

    protected function makeEloquent()
    {
        $name = $this->argument('name');

        if ($this->files->exists($path = $this->getEloquentPath($name)))
        {

            return $this->error('Eloquent already exists!');

        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->compileEloquentStub());

        $this->info('Eloquent '.$name.' created successfully.');

    }

    protected function makeRepository()
    {
        $name = $this->argument('name');

        if ($this->files->exists($path = $this->getRepositoryPath($name)))
        {

            return $this->error('Repository already exists!');

        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->compileRepositoryStub());

        $this->info('Repository '.$name.' created successfully.');

    }

    protected function getEloquentPath($name)
    {

        $name = $this->argument('name');

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true")
        {

            return base_path() . '/app/Codes/Repositories/' . str_plural($name) . '/Eloquent' . str_singular($name) . '.php';

        }

        return base_path() . '/app/Codes/Repositories/' . str_singular($name) . '/Eloquent' . str_singular($name) . '.php';

    }

    protected function getRepositoryPath($name)
    {

        $name = $this->argument('name');

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true")
        {

            return base_path() . '/app/Codes/Repositories/' . str_plural($name) . '/' . str_singular($name) . 'Repository.php';

        }

        return base_path() . '/app/Codes/Repositories/' . str_singular($name) . '/' . str_singular($name) . 'Repository.php';

    }

    protected function compileEloquentStub()
    {

        $stub = $this->files->get(__DIR__ . '/../stubs/eloquent.stub');

        $this->replaceEloquentClassName($stub)

            ->replaceNameSpace($stub);

        return $stub;

    }

    protected function compileRepositoryStub()
    {

        $stub = $this->files->get(__DIR__ . '/../stubs/repository.stub');

        $this->replaceRepositoryName($stub)

            ->replaceNameSpace($stub);

        return $stub;

    }

    protected function replaceEloquentClassName(&$stub)
    {

        $name = $this->argument('name');

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        $className = 'Eloquent'.str_singular($name);

        $repositoryName = ucwords(str_singular($name)).'Repository';

        $stub = str_replace('{{eloquent_class}}', $className, $stub);

        $stub = str_replace('{{eloquent_repository}}', $repositoryName, $stub);

        return $this;
    }

    protected function replaceRepositoryName(&$stub)
    {

        $name = $this->argument('name');

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        $repositoryName = str_singular($name).'Repository';

        $stub = str_replace('{{repository_name}}', $repositoryName, $stub);

        return $this;
    }

    protected function replaceNameSpace(&$stub)
    {

        $name = $this->argument('name');

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true")
        {

            $nameSpace = 'namespace App\Codes\Repositories\\' . str_plural($name);

        }
        else
        {

            $nameSpace = 'namespace App\Codes\Repositories\\' . str_singular($name);

        }

        $stub = str_replace('{{namespace}}', $nameSpace, $stub);

        return $this;
    }

    protected function makeProvider()
    {

        $name = $this->argument('name');

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);



        if ($this->files->exists($path = $this->getProviderPath($name)))
        {

            return $this->error('Provider already exists!');

        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->compileProviderStub());

        $tag = "/*CUSTOM SERVICE PROVIDERS*/";

        $init_tag = "/*CUSTOM SERVICE PROVIDERS*/ \n\n";

        $content_path = base_path().'/bootstrap/app.php';

        $content = $this->files->get($content_path);

        $text = $init_tag;

        if($this->option('plural') == "true") {

            $text .= '$app->register(\App\Codes\Providers\\' . str_plural($name) . '\Provider::class);';

        }else{

            $text .= '$app->register(\App\Codes\Providers\\' . str_singular($name) . '\Provider::class);';

        }

        $content = str_replace($tag,$text,$content);

        $this->files->delete($content_path);

        $this->files->put($content_path,$content);

        $this->info('Provider '.$name.' created successfully.');

    }

    protected function getProviderPath($name)
    {

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true")
        {

            return base_path() . '/app/Codes/Providers/' . ucwords(str_plural($name)) . '/Provider.php';

        }

        return base_path() . '/app/Codes/Providers/' . ucwords(str_singular($name)) . '/Provider.php';

    }

    protected function compileProviderStub()
    {
        $stub = $this->files->get(__DIR__ . '/../stubs/provider.stub');

        $this->replaceProvider($stub);

        return $stub;
    }

    protected function replaceProvider(&$stub)
    {

        $name = $this->argument('name');

        $name = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {

            return ucwords($matches[1]);

        }, $name);

        if($this->option('plural') == "true")
        {

            $namespace = 'namespace App\Codes\Providers\\' . ucwords(str_plural($name));

            $folder_name = ucwords(str_plural($name));

        }else{

            $namespace = 'namespace App\Codes\Providers\\' . ucwords(str_singular($name));

            $folder_name = ucwords(str_singular($name));

        }

        $repository_name = ucwords(str_singular($name)).'Repository';

        $eloquent_class = 'Eloquent'.str_singular($name);

        $class_model = str_singular($name);

        $stub = str_replace('{{provider_namespace}}', $namespace, $stub);

        $stub = str_replace('{{folder_name}}', $folder_name, $stub);

        $stub = str_replace('{{class_model}}', $class_model, $stub);

        $stub = str_replace('{{eloquent_class}}', $eloquent_class, $stub);

        $stub = str_replace('{{repository_name}}', $repository_name, $stub);

        return $this;

    }

    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the scaffolding'],
        ];
    }

    protected function getOptions()
    {
        return [
            ['plural', 'null', InputOption::VALUE_OPTIONAL, 'Optional schema to be attached to the scaffold', null],
//            ['model', null, InputOption::VALUE_OPTIONAL, 'Want a model for this table?', true],
        ];
    }
}