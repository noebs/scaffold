<?php

namespace Noebs\Scaffold;

use Illuminate\Support\ServiceProvider;

class ScaffoldServiceProvider extends ServiceProvider
{

    public function boot()
    {
        //
    }

    public function register()
    {

        $this->registerScaffoldGenerator();

    }

    private function registerScaffoldGenerator()
    {
        $this->app->singleton('command.noebs.scaffold', function ($app) {
            return $app['Noebs\Scaffold\Commands\ScaffoldMakeCommand'];
        });

        $this->commands('command.noebs.scaffold');
    }

}