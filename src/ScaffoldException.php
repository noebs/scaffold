<?php

namespace Noebs\Scaffold;

class ScaffoldException extends \Exception
{

    protected $message = 'Could not determine what you are trying to do.';

}